/**
 *   \file camera_properties.cpp
 *   \brief Class for storing camera properties and calibrating the camera
 *
 *  Detailed description
 *
 */
#include <iostream>
#include <sstream>
#include <opencv2/opencv.hpp>
#include <memory>
using namespace cv;
using namespace std;

class CameraProperties
{
 public:
  //! Default constructor
  CameraProperties(bool fromFile_=false): fromFile(fromFile_),
                                          cam_id(0),
                                          camera_width(1280),
                                          camera_height(480),
                                          board_width(7),
                                          board_height(5),
                                          nr_images(20),
                                          //folder("/home/sgj/workspace/gaze-vergence-control/src/visual_odometry/calib_images") {
                                          folder("/home/sgj/workspace/gaze-vergence-control/src/camera_calib/calib_images_elp_l_720p_temp"){
    /*Default initializer list*/
    capture.open(cam_id);
  }

  CameraProperties(int cam_id,
                   int camera_width,
                   int camera_height,
                   int board_width,
                   int board_height,
                   int nr_images,
                   std::string folder):cam_id(cam_id),
                                       camera_width(camera_width),
                                       camera_height(camera_height),
                                       board_width(board_width),
                                       board_height(board_height),
                                       nr_images(nr_images),
                                       folder(folder) {
    /*Another initializer list*/
    capture.open(cam_id);
  }

  int cam_id;
  int camera_width;
  int camera_height;
  int board_width;
  int board_height;
  int nr_images;
  std::string folder;
  vector<vector<Point2f>> image_points;
  vector<vector<Point3f>> object_points;
  Mat intrinsic_matrix;
  Mat distortion_coeffs;
  VideoCapture capture;
  bool fromFile;
  Mat jointImage;
  Mat map1;
  Mat map2;

  int init_camera() {
    if (!capture.isOpened()) {
      cout << "Couldn't open the camera\n";
      return -1;
    }
    capture.set(CV_CAP_PROP_FRAME_WIDTH, camera_width);
    capture.set(CV_CAP_PROP_FRAME_HEIGHT, camera_height);
    camera_width = static_cast<int>(capture.get(CV_CAP_PROP_FRAME_WIDTH));
    camera_height = static_cast<int>(capture.get(CV_CAP_PROP_FRAME_HEIGHT));
    namedWindow("Camera_feed", CV_WINDOW_AUTOSIZE);
    return 0;
  }

  int splitStereoCamFeed(Mat& right, Mat& left, int fileIndex = 0) {
    Mat image;
    stringstream ss;
    cv::Rect  left_roi(0, 0 , camera_height, camera_width),
        right_roi(camera_height, camera_width, camera_height*2, camera_width*2);
    //init_camera();
    if (!fromFile) {
      bool x = capture.read(image);
      if(!x) {
        cout << "Image not read form Camera"  << "\n";
      }
    }
    else {
      ss << folder << "/" << fileIndex << ".png";
      string fn = ss.str();
      // cout << "retrieving image from file " << fn << i << endl;
      image = imread(fn.c_str());
    }
    imshow("Camera_feed", image);
    waitKey(10);
    jointImage = image;
    left = image( Rect(0, 0, camera_width/2, camera_height));
    right = image( Rect(camera_width/2, 0, camera_width/2,  camera_height));
    //cout << "Done splitting"  << "\n";

    #if 1
    // saving images //////////////////////////////////////////////////////////
    Size board_sz = Size(board_width, board_height);
    // find corners on the chess board
    vector<cv::Point2f> corners;
    cout << "findChessboardCorners stared"  << "\n";
    bool found_l = cv::findChessboardCorners(left, board_sz, corners);
    bool found_r = cv::findChessboardCorners(right, board_sz, corners);

    if(found_l && found_r ) {
      string fn;
      if(!fromFile) {
        stringstream ss, ss2;
        ss << folder << "/" << "left" << fileIndex << ".png";
        fn = ss.str();
        imwrite(fn.c_str(), left);
        ss2 << folder << "/" << "right" << fileIndex << ".png";
        fn = ss2.str();
        imwrite(fn.c_str(), right);
        cout << "image " << fileIndex << " obtained: " << fn << endl;
      }

    }

    #endif
  }

  bool saveImage(int fileIndex_ = 0, std::string str = "") {
    string fn;
    if(!fromFile) {
      stringstream ss;
      ss << folder << "/" << fileIndex_ << ".png";
      fn = ss.str();
      imwrite(fn.c_str(), jointImage);
      cout << "image " << fileIndex_ << " obtained: " << fn << endl;
    }
  }

  bool findCornersWrapper(Mat image) {
    int board_size = board_width * board_height;
    Size board_sz = Size(board_width, board_height);
    // if (!fromFile) init_camera();
    cout << "Camera opened: " << camera_width << " * " << camera_height << endl;
    namedWindow("Calibration", CV_WINDOW_AUTOSIZE);
    // find corners on the chess board
    vector<cv::Point2f> corners;
    cout << "findChessboardCorners stared"  << "\n";
    bool found = cv::findChessboardCorners(image, board_sz, corners);
    cout << "findChessboardCorners done"  << "\n";
    if (found) {
      image_points.push_back(corners);
      vector<Point3f> object_point;
      for (int j = 0; j < board_size; j++) {
        float x = (float) (j / board_width);
        float y = (float) (j % board_width);
        float z = 0.0;
        Point3f p(x, y, z);
        object_point.push_back(p);
      }
      object_points.push_back(object_point);
      drawChessboardCorners(image, board_sz, corners, found);
    }

    imshow("Calibration", image);
    cout << "Done 5"  << "\n";
    waitKey(1000);
    destroyWindow("Calibration");
    //capture.release();
    return found;
  }

  void computeCameraParameters() {
    cout << "Compute camera params"  << "\n";
    cv::Size image_size(camera_width, camera_height);

    double err = cv::calibrateCamera(
        object_points,
        image_points,
        image_size,
        intrinsic_matrix, // ret value
        distortion_coeffs, // ret value
        cv::noArray(),
        cv::noArray(),
        0);
    cout << "Compute camera parameters done. err = " << err << endl;

  }

  void computeRectificationParams() {
    Size image_size(camera_width, camera_height);
    cout << "Computing rectification params"  << "\n";

    cv::initUndistortRectifyMap(
        intrinsic_matrix,
        distortion_coeffs,
        Mat(),
        intrinsic_matrix,
        image_size,
        CV_16SC2,
        map1,
        map2);
  }

  cv::Mat rectifyImage(Mat image) {
    Mat imageRectified;
    remap(image, imageRectified, map1, map2, CV_INTER_LINEAR,
          BORDER_CONSTANT, Scalar());
#if 0
    namedWindow("RectifiedCameraFeed", CV_WINDOW_AUTOSIZE);
    imshow("RectifiedCameraFeed", imageRectified);
    waitKey(10);
#endif
    return imageRectified;
  }

  void saveCameraPramasToFile(const std::string& filename) {
    // nothing for now!!
    Size image_size(camera_width, camera_height);
    FileStorage fs( filename.c_str(), FileStorage::WRITE);
    fs << "camera_width" << image_size.width
       << "camera_height" << image_size.height
       << "intrinsic_matrix" << intrinsic_matrix
       << "distortion_coefficients" << distortion_coeffs
       << "map1" << map1
       << "map2" << map2;
    fs.release();
  }

  void loadCameraParamsFromFile(const std::string& filename) {
    FileStorage fs;
    fs.open( filename.c_str(), FileStorage::READ);
    camera_width = static_cast<int>(fs["camera_width"]);
    camera_height = static_cast<int>(fs["camera_height"]);
    fs["intrinsic_matrix"] >> intrinsic_matrix;
    fs["distortion_coefficients"] >> distortion_coeffs;
    fs["map1"] >> map1;
    fs["map2"] >> map2;
    cout << "camera_width: " << camera_width << "\n";
    cout << "camera_height: " << camera_height << "\n";
    cout << "intrinsic matrix:" << intrinsic_matrix << "\n";
    cout << "distortion coefficients: " << distortion_coeffs << endl;
    //    cout << "map1" << map1 << "\n";
    //                            cout << "map2" << map2  << "\n";
  }

 protected:
 private:
};


int main(int argc, char *argv[])
{

#if 0
  // calibration and rectification ////////////////////////////////////////////
  {  //  std::cout << "do Nothing" << '\n';
    std::shared_ptr<CameraProperties> left = std::make_shared<CameraProperties>();
    std::shared_ptr<CameraProperties> right = std::make_shared<CameraProperties>();
    //  left.findCornersWrapper(true);
    //  left.computeCameraParameters();
    left->init_camera();
    right->init_camera();
    int count = 0, cornerCount = 0;
    while (cornerCount < 5) {

      Mat right_img, left_img;
      bool foundLeftCorners, foundRightCorners;
      count ++;
      left->splitStereoCamFeed(right_img, left_img);
      if(count % 100 == 0) {
        foundLeftCorners = left->findCornersWrapper(left_img);
        foundRightCorners = right->findCornersWrapper(right_img);
        count = 0;
        if (foundRightCorners && foundLeftCorners) {
          cornerCount ++;
          left->saveImage(cornerCount);
          cout << "saving file completed"  << "\n";
        }
      }

    }

    left->computeCameraParameters();
    right->computeCameraParameters();

    left->computeRectificationParams();
    right->computeRectificationParams();

    left->saveCameraPramasToFile("elp_left.xml");
    right->saveCameraPramasToFile("elp_right.xml");
  }
#endif

#if 0
  // calibration loaded from file, only rectification /////////////////////////
  const std::string paramFile_l = "elp_left.xml", paramFile_r = "elp_right.xml";

  //CameraProperties left2(false);
  CameraProperties right2(false);
  CameraProperties left2(false);
  right2.loadCameraParamsFromFile(paramFile_r );
  left2.loadCameraParamsFromFile(paramFile_l );
  right2.init_camera();

  Mat leftRectified, rightRectified, grayL, grayR;
  vector<Point2f> points[2];
  vector<Point2f> pointsR[2];
  Size subPixWinSize(10,10), winSize(63,63);
  TermCriteria termcrit(TermCriteria::COUNT|TermCriteria::EPS,20,0.03);
  bool toInit = true, toInitR = true;
  namedWindow( "LK Demo Left", CV_WINDOW_AUTOSIZE);
  namedWindow( "LK Demo Right", CV_WINDOW_AUTOSIZE);
  while(true) {
    Mat rightImg, leftImg, prevGrayL, prevGrayR;
    right2.splitStereoCamFeed(rightImg, leftImg);
#if 0
    namedWindow("UnRectifiedCameraFeed", CV_WINDOW_AUTOSIZE);
    imshow("UnRectifiedCameraFeed", rightImg);
#endif
    leftRectified = left2.rectifyImage(leftImg);
    rightRectified = right2.rectifyImage(rightImg);
    // feature tracking ///////////////////////////////////////////////////////
    cvtColor(leftRectified, grayL, COLOR_BGR2GRAY);
    cvtColor(rightRectified, grayR, COLOR_BGR2GRAY);
    // left camera optical flow ///////////////////////////////////////////////
    if(toInit) {
      goodFeaturesToTrack(grayL, points[1], 500, 0.001, 20, Mat(), 3, 3, 0, 0.04);
      cornerSubPix(grayL, points[1], subPixWinSize, Size(-1,-1), termcrit);
      toInit = false;
      cout << "Feature tracking Corner's Init Done" << "\n";
    }
    else if (!points[0].empty()) {
      vector<uchar> status;
      vector<float> err;
      if(prevGrayL.empty())
        grayL.copyTo(prevGrayL);
      calcOpticalFlowPyrLK(prevGrayL, grayL, points[0], points[1], status, err, winSize,
                           6, termcrit, 0, 0.001);
      //      cout << "Optical flow calc done" << "\n";
      size_t i, k;
      for( i = k = 0; i < points[1].size(); i++ ) {
        if( !status[i] ) {
          cout << "point dropped Left"  << "\n";
          continue;
        }
        points[1][k++] = points[1][i];
        circle( leftRectified, points[1][i], 3, Scalar(0,255,0), -1, 8);
      }
      points[1].resize(k);
    }
    imshow("LK Demo Left", leftRectified);
    std::swap(points[1], points[0]);
    cv::swap(prevGrayL, grayL);

    // right camera optical flow //////////////////////////////////////////////
    if(toInitR) {
      goodFeaturesToTrack(grayR, pointsR[1], 500, 0.001, 20, Mat(), 3, 3, 0, 0.04);
      cornerSubPix(grayR, pointsR[1], subPixWinSize, Size(-1,-1), termcrit);
      toInitR = false;
      cout << "Feature tracking Corner's Init Done" << "\n";
    }
    else if (!pointsR[0].empty()) {
      vector<uchar> status;
      vector<float> err;
      if(prevGrayR.empty())
        grayR.copyTo(prevGrayR);
      calcOpticalFlowPyrLK(prevGrayR, grayR, pointsR[0], pointsR[1], status, err, winSize,
                           6, termcrit, 0, 0.001);
      //      cout << "Optical flow calc done" << "\n";
      size_t i, k;
      for( i = k = 0; i < pointsR[1].size(); i++ ) {
        if( !status[i] ) {
          cout << "point dropped Right"  << "\n";
          continue;
        }
        pointsR[1][k++] = pointsR[1][i];
        circle( rightRectified, pointsR[1][i], 3, Scalar(0,255,0), -1, 8);
      }
      pointsR[1].resize(k);
    }

    imshow("LK Demo Right", rightRectified);
    std::swap(pointsR[1], pointsR[0]);
    cv::swap(prevGrayR, grayR);

    // undistort points  left camera //////////////////////////////////////////
    vector<Point2f> undistortedPointsLeft;
    cv::undistortPoints(points[0], undistortedPointsLeft, left2.intrinsic_matrix, left2.distortion_coeffs);

    // undistort points  left camera //////////////////////////////////////////
    vector<Point2f> undistortedPointsRight;
    cv::undistortPoints(pointsR[0], undistortedPointsRight, right2.intrinsic_matrix, right2.distortion_coeffs);
    // comment ////////////////////////////////////////////////////////////////////
  }
  //namedWindow("Calibration_left", CV_WINDOW_AUTOSIZE);
  //imshow("Calibration_left", left_img);
#endif

  // save iamges from camera feed /////////////////////////////////////////////
#if 1
    std::shared_ptr<CameraProperties> left = std::make_shared<CameraProperties>();
    std::shared_ptr<CameraProperties> right = std::make_shared<CameraProperties>();
    //  left.findCornersWrapper(true);
    //  left.computeCameraParameters();
    left->init_camera();
    int count = 0, cornerCount = 0;
    while (count < 6) {
      Mat right_img, left_img;
      bool foundLeftCorners, foundRightCorners;
      count ++;
      left->splitStereoCamFeed(right_img, left_img, count);
      waitKey(100);

    }
#endif
  return 0;
}
