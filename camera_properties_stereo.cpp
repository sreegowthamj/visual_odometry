/**
 *   \file camera_properties.cpp
 *   \brief Class for storing camera properties and calibrating a stereo camera
 *
 *  Detailed description
 *
 */
#include <ctype.h>
#include <iostream>
#include <math.h>
#include <memory>
#include <opencv2/opencv.hpp>
#include <sstream>
using namespace std;

struct PointDistancePair {
    double distance;
    cv::Point2f point;
};

void help(char* argv[])
{
    cout << "stereo calibration"
         << "\n";
}

static double cross_product(cv::Point a, cv::Point b)
{
    return a.x * b.y - a.y * b.x;
}

static double distance_to_line2(
    const cv::Point& p, const cv::Vec3f& line_vec,
    int num_of_cols /*cv::Point begin, cv::Point end, cv::Point x*/)
{
    cv::Point begin, end, temp_p;
    temp_p = p;

    begin = cv::Point(0, -line_vec[2] / line_vec[1]);
    end   = cv::Point(num_of_cols,
        -(line_vec[2] + line_vec[0] * num_of_cols) / line_vec[1]);

    // translate the begin to the origin
    end -= begin;
    temp_p = temp_p - begin;
    //¿do you see the triangle?
    double area = cross_product(temp_p, end);
    return area / norm(end);
}

static void get_stereo_corner_correspondence(
    const vector<cv::Point2f>& points_tracked_l,
    const vector<cv::Point2f>& points_tracked_r, const cv::Mat& F, int cols,
    const cv::Mat& image_l, const cv::Mat& image_r)
{
    if (!points_tracked_l.empty() && !points_tracked_r.empty()) {
        cout << "Matching features"
             << "\n";
        std::vector<cv::Vec3f> lines1, lines2;
        cv::computeCorrespondEpilines(points_tracked_l, 1, F, lines1);
        cv::computeCorrespondEpilines(points_tracked_r, 2, F, lines2);
        std::vector<PointDistancePair> distance_line2_point1;
        int count = 0;
        cv::Point epipolar_line_point;
        cout << "points_tracked_r size = " << points_tracked_r.size() << "\n";
        for (std::vector<cv::Vec3f>::const_iterator it = lines2.begin();
             it != lines2.end(); ++it) {
            epipolar_line_point = points_tracked_r.at(count);
            for (std::vector<cv::Point2f>::const_iterator it2 = points_tracked_r.begin();
                 it2 != points_tracked_r.end(); ++it2) {
                double distance1 = abs(distance_to_line2(*it2, *it, cols));
                //        cout << " distance1 = " << distance1 << "\n";
                if (distance1 <= 3 && distance1 >= 0) {
                    PointDistancePair x;
                    x.point    = *it2;
                    x.distance = distance1;
                    distance_line2_point1.push_back(x);
                }
                //        cout << "distance_line2_point size" << distance_line2_point1.size()
                //             << "\n";
            }
            //      cout << "Plotting points on image"
            //           << "\n";
            cv::Point line_start, line_end;
            line_start           = cv::Point(0, -(*it)[2] / (*it)[1]);
            line_end             = cv::Point(cols, -((*it)[2] + (*it)[0] * cols) / (*it)[1]);
            cv::Mat image_r_temp = image_r.clone();
            cv::Mat image_l_temp = image_l.clone();
            cv::line(image_r_temp, cv::Point(0, -(*it)[2] / (*it)[1]),
                cv::Point(cols, -((*it)[2] + (*it)[0] * cols) / (*it)[1]),
                cv::Scalar(255, 255, 255));
            cv::circle(image_r_temp, line_start, 6, cv::Scalar(0, 255, 0), -1, 8);
            cv::circle(image_r_temp, line_end, 6, cv::Scalar(0, 255, 0), -1, 8);
            cv::circle(image_l_temp, epipolar_line_point, 6, cv::Scalar(0, 0, 255),
                -1, 8);
            for (std::vector<PointDistancePair>::const_iterator x = distance_line2_point1.begin();
                 x != distance_line2_point1.end(); ++x) {
                cv::circle(image_r_temp, x->point, 6, cv::Scalar(255, 0, 0), -1, 8);
            }
            imshow("image with possible matches", image_r_temp);
            imshow("Point corresponding to epipolar line", image_l_temp);
            cv::waitKey(0);
            distance_line2_point1.clear();
            count++;
        }
    }
}

static void StereoCalib(const char* imageList, int nx, int ny,
    bool useUncalibrated)
{
    bool displayCorners    = true;
    bool showUndistorted   = true;
    bool isVerticalStereo  = false; // horiz or vert cams
    const int maxScale     = 1;
    const float squareSize = 1.f;

    // actual square size
    FILE* f = fopen(imageList, "rt");
    int i, j, lr;
    int N             = nx * ny;
    cv::Size board_sz = cv::Size(nx, ny);
    vector<string> imageNames;
    vector<cv::Point3f> boardModel;
    vector<vector<cv::Point3f>> objectPoints;
    vector<vector<cv::Point2f>> points[2];
    vector<cv::Point2f> corners[2];
    bool found[2] = { false, false };
    cv::Size imageSize;
    cv::Size tempSize;
    int camera_height = 480, camera_width = 1280;

    // READ IN THE LIST OF CIRCLE GRIDS:
    //
    if (!f) {
        cout << "Cannot open file " << imageList << endl;
        return;
    }
    for (i = 0; i < ny; i++)
        for (j = 0; j < nx; j++)
            boardModel.push_back(
                cv::Point3f((float)(i * squareSize), (float)(j * squareSize), 0.f));
    i = 0;

    // read the stereo-images from a set of cameras /////////////////////////////
    for (;;) {
        char buf[1024];
        //    lr = i % 2;
        //    if (lr == 0)
        //      found[0] = found[1] = false;
        if (!fgets(buf, sizeof(buf) - 3, f))
            break;
        size_t len = strlen(buf);
        while (len > 0 && isspace(buf[len - 1]))
            buf[--len] = '\0';
        if (buf[0] == '#')
            continue;
        cv::Mat img = cv::imread(buf, 0);
        if (img.empty())
            break;
        imageSize = img.size();
        tempSize  = cv::Size(imageSize.width / 2, imageSize.height);

        imageNames.push_back(buf);
        i++;

        for (int s = 1; s <= maxScale; s++) {
            cv::Mat timg = img;
            if (s > 1)
                resize(img, timg, cv::Size(), s, s, cv::INTER_CUBIC);
            cv::Mat left  = timg(cv::Rect(0, 0, camera_width / 2, camera_height));
            cv::Mat right = timg(cv::Rect(camera_width / 2, 0, camera_width / 2, camera_height));
            found[0]      = cv::findChessboardCorners(left, board_sz, corners[0]);
            found[1]      = cv::findChessboardCorners(right, board_sz, corners[1]);

            if ((found[0] && found[1]) || s == maxScale) {
                cv::Mat mcorners_l(corners[0]);
                cv::Mat mcorners_r(corners[1]);
                mcorners_l *= (1. / s);
                mcorners_r *= (1. / s);
            }
            if (found[0] && found[1])
                break;
        }
        if (displayCorners && found[0] && found[1]) {
            cout << buf << endl;
            cv::Mat cimg;
            cv::cvtColor(img, cimg, cv::COLOR_GRAY2BGR);

            // draw chessboard corners works for circle grids too
            cv::Mat cimg_l = cimg(cv::Rect(0, 0, camera_width / 2, camera_height));
            cv::Mat cimg_r = cimg(cv::Rect(camera_width / 2, 0, camera_width / 2, camera_height));
            cv::drawChessboardCorners(cimg_l, cv::Size(nx, ny), corners[0], found[0]);
            cv::drawChessboardCorners(cimg_r, cv::Size(nx, ny), corners[1], found[1]);
            cv::imshow("Corners left image", cimg_l);
            cv::imshow("Corners right image", cimg_r);
            if ((cv::waitKey(0) & 255) == 27) // Allow ESC to quit
                exit(-1);
        } else
            cout << '.';
        if (found[0] && found[1]) {
            objectPoints.push_back(boardModel);
            points[0].push_back(corners[0]);
            points[1].push_back(corners[1]);
        }
    }
    fclose(f);
    // CALIBRATE THE STEREO CAMERAS
    cv::Mat M1 = cv::Mat::eye(3, 3, CV_64F);
    cv::Mat M2 = cv::Mat::eye(3, 3, CV_64F);
    cv::Mat D1, D2, R, T, E, F;
    cout << "\nRunning stereo calibration ...\n";
    cv::stereoCalibrate(
        objectPoints, points[0], points[1], M1, D1, M2, D2, tempSize, R, T, E, F,
        cv::CALIB_FIX_ASPECT_RATIO | cv::CALIB_ZERO_TANGENT_DIST | cv::CALIB_SAME_FOCAL_LENGTH,
        cv::TermCriteria(cv::TermCriteria::COUNT | cv::TermCriteria::EPS, 1000,
            1e-5));
    cout << "Calibration Done! \n\n  \n\n";
    // CALIBRATION QUALITY CHECK
    // because the output fundamental matrix implicitly
    // includes all the output information,
    // we can check the quality of calibration using the
    // epipolar geometry constraint: m2^t*F*m1=0
    vector<cv::Point3f> lines[2];
    double avgErr = 0;
    int nframes   = (int)objectPoints.size();
    for (i = 0; i < nframes; i++) {
        vector<cv::Point2f>& pt0 = points[0][i];
        vector<cv::Point2f>& pt1 = points[1][i];
        cv::undistortPoints(pt0, pt0, M1, D1, cv::Mat(), M1);
        cv::undistortPoints(pt1, pt1, M2, D2, cv::Mat(), M2);
        cv::computeCorrespondEpilines(pt0, 1, F, lines[0]);
        cv::computeCorrespondEpilines(pt1, 2, F, lines[1]);
        for (j = 0; j < N; j++) {
            double err = fabs(pt0[j].x * lines[1][j].x + pt0[j].y * lines[1][j].y + lines[1][j].z) + fabs(pt1[j].x * lines[0][j].x + pt1[j].y * lines[0][j].y + lines[0][j].z);
            avgErr += err;
        }
    }
    cout << "avg err = " << avgErr / (nframes * N) << endl;
    // COMPUTE AND DISPLAY RECTIFICATION
    //
    if (showUndistorted) {
        cout << "Show undistorted is true"
             << "\n";
        cv::Mat R1, R2, P1, P2, map11, map12, map21, map22;
        cv::Rect validPixROI1, validPixROI2;
        cv::Size tempSize(imageSize.width / 2, imageSize.height), newImageSize(0, 0);

        cv::stereoRectify(M1, D1, M2, D2, tempSize, R, T, R1, R2, P1, P2, cv::noArray(), cv::CALIB_ZERO_DISPARITY, 0, newImageSize, &validPixROI1, &validPixROI2);
        isVerticalStereo = fabs(P2.at<double>(1, 3)) > fabs(P2.at<double>(0, 3));
        // Precompute maps for cvRemap()
        initUndistortRectifyMap(M1, D1, R1, P1, tempSize, CV_16SC2, map11, map12);
        initUndistortRectifyMap(M2, D2, R2, P2, tempSize, CV_16SC2, map21, map22);
        // RECTIFY THE IMAGES AND FIND DISPARITY MAPS
        //
        cv::Mat pair;
        if (!isVerticalStereo)
            pair.create(imageSize.height, imageSize.width, CV_8UC3);
        else {
            cout << "Vertical Stereo pair, Exiting"
                 << "\n";
            exit(-1);
        }
        // Setup for finding stereo corrrespondences
        // in the checker board images
        cv::Ptr<cv::StereoSGBM> stereo = cv::StereoSGBM::create(
            -64, 128, 11, 100, 1000, 32, 0, 15, 1000, 16, cv::StereoSGBM::MODE_HH);

        for (i = 0; i < nframes; i++) {
            cout << "nframes = " << nframes << ", i = " << i << "\n";
            cv::Mat img = cv::imread(imageNames[i].c_str(), 0);
            // cv::Mat img2 = cv::imread(imageNames[i].c_str(), 0);
            cv::Mat img1 = img(cv::Rect(0, 0, camera_width / 2, camera_height));
            cv::Mat img2 = img(cv::Rect(camera_width / 2, 0, camera_width / 2, camera_height));
            cv::rectangle(img1, validPixROI1, cv::Scalar(255, 255, 255), 10, 8);
            cv::imshow("left image test op", img1);
            cv::imshow("right image test op", img2);
            cv::Mat img1r, img2r, disp, vdisp;
            if (img1.empty() || img2.empty())
                continue;
            cv::remap(img1, img1r, map11, map12, cv::INTER_LINEAR);
            cv::remap(img2, img2r, map21, map22, cv::INTER_LINEAR);
            if (!isVerticalStereo || !useUncalibrated) {

                // When the stereo camera is oriented vertically,
                // Hartley method does not transpose the
                // image, so the epipolar lines in the rectified
                // images are vertical. Stereo correspondence
                // function does not support such a case.
                stereo->compute(img1r, img2r, disp);
                cv::normalize(disp, vdisp, 0, 256, cv::NORM_MINMAX, CV_8U);
                cv::imshow("disparity", vdisp);
                cv::waitKey(100);
            }
            cv::Mat part = pair.colRange(0, imageSize.width / 2);
            cvtColor(img1r, part, cv::COLOR_GRAY2BGR);
            part = pair.colRange(imageSize.width / 2, imageSize.width);
            cvtColor(img2r, part, cv::COLOR_GRAY2BGR);
            for (j = 0; j < imageSize.height; j += 16)
                cv::line(pair, cv::Point(0, j), cv::Point(imageSize.width, j),
                    cv::Scalar(0, 255, 0));
            cv::imshow("rectified", pair);
            cv::imshow("part1 img", img1);
            cv::imshow("part1 img rectified", img1r);
            cv::waitKey();
            //            break;
        }
    }
    /////////////////////////////////////////////////////////////////////////////
    //                       Optical flow implementation                       //
    /////////////////////////////////////////////////////////////////////////////
    cout << " Starting optical flow"
         << "\n";
    int input_channel = 1;
    cv::VideoCapture cap;
    cap.open(input_channel);
    if (!cap.isOpened()) {
        cout << "Could not initialize capturing...\n";
        return;
    }
    cv::TermCriteria termcrit(cv::TermCriteria::COUNT | cv::TermCriteria::EPS,
        2000, 0.0003);
    cv::Size subPixWinSize(10, 10), winSize(31, 31);
    const int MAX_COUNT = 1000;
    bool needToInit     = true;
    cv::Point2f point;
    bool addRemovePt = false;
    cv::Mat prevGray_l, prevGray_r, image, image_l, image_r, frame;
    vector<cv::Point2f> points_tracked_l[2], points_tracked_r[2];
    cv::Mat gray_left, gray_right;

    for (;;) {
        cap >> frame;
        cv::Size frame_size = frame.size();
        if (frame.empty())
            break;
        frame.copyTo(image);
        image_l = image(cv::Rect(0, 0, frame_size.width / 2, frame_size.height));
        image_r = image(cv::Rect(frame_size.width / 2, 0, frame_size.width / 2,
            frame_size.height));

        cv::Mat temp_l, temp_r;
        cv::undistort(image_l, temp_l, M1, D1);
        cv::undistort(image_r, temp_r, M1, D1);
        temp_l.copyTo(image_l);
        temp_r.copyTo(image_r);

        cv::cvtColor(image_l, gray_left, cv::COLOR_BGR2GRAY);
        cv::cvtColor(image_r, gray_right, cv::COLOR_BGR2GRAY);
        cv::waitKey(10);
        //    gray_left = gray(cv::Rect(0, 0, frame_size.width/2,
        //    frame_size.height)); gray_right = gray(cv::Rect(frame_size.width/2, 0,
        //    frame_size.width/2,  frame_size.height));
        cv::imshow("test window left", gray_left);
        cv::imshow("test window right", gray_right);

        if (needToInit) {
            // automatic initialization
            goodFeaturesToTrack(gray_left, points_tracked_l[1], MAX_COUNT, 0.01, 10,
                cv::Mat(), 3, 3, 0, 0.04);
            goodFeaturesToTrack(gray_right, points_tracked_r[1], MAX_COUNT, 0.01, 10,
                cv::Mat(), 3, 3, 0, 0.04);
            cornerSubPix(gray_left, points_tracked_l[1], subPixWinSize,
                cv::Size(-1, -1), termcrit);
            cornerSubPix(gray_right, points_tracked_r[1], subPixWinSize,
                cv::Size(-1, -1), termcrit);
            addRemovePt = false;
            needToInit  = false;
            cout << "Init done for goodFeaturesTotrack"
                 << "\n";
            cout << "number of features in left and right = "
                 << points_tracked_l[1].size() << ", " << points_tracked_r[1].size()
                 << "\n";
        } else if (!points_tracked_l[0].empty() && !points_tracked_r[0].empty()) {
            vector<uchar> status_r, status_l;
            vector<float> err_r, err_l;
            if (prevGray_l.empty())
                gray_left.copyTo(prevGray_l);
            if (prevGray_r.empty())
                gray_right.copyTo(prevGray_r);
            calcOpticalFlowPyrLK(prevGray_l, gray_left, points_tracked_l[0],
                points_tracked_l[1], status_l, err_l, winSize, 3,
                termcrit, 0, 0.001);
            calcOpticalFlowPyrLK(prevGray_r, gray_right, points_tracked_r[0],
                points_tracked_r[1], status_r, err_r, winSize, 3,
                termcrit, 0, 0.001);
            size_t i, k;
            for (i = k = 0; i < points_tracked_l[1].size(); i++) {
                if (!status_l[i]) {
                    cout << " point dropped left"
                         << "\n";
                    continue;
                }
                points_tracked_l[1][k++] = points_tracked_l[1][i];
                cv::circle(image_l, points_tracked_l[1][i], 3, cv::Scalar(0, 255, 0),
                    -1, 8);
            }
            points_tracked_l[1].resize(k);

            for (i = k = 0; i < points_tracked_r[1].size(); i++) {
                if (!status_r[i]) {
                    cout << " point dropped right"
                         << "\n";
                    continue;
                }

                points_tracked_r[1][k++] = points_tracked_r[1][i];
                cv::circle(image_r, points_tracked_r[1][i], 3, cv::Scalar(0, 255, 0),
                    -1, 8);
            }
            points_tracked_r[1].resize(k);
            cout << "number of features in left and right = "
                 << points_tracked_l[1].size() << ", " << points_tracked_r[1].size()
                 << "\n";
        }
        needToInit = false;
        imshow("LK Demo left", image_l);
        imshow("LK Demo right", image_r);
        std::swap(points_tracked_l[1], points_tracked_l[0]);
        cv::swap(prevGray_l, gray_left);
        std::swap(points_tracked_r[1], points_tracked_r[0]);
        cv::swap(prevGray_r, gray_right);
        cv::destroyAllWindows();

        // matching features //////////////////////////////////////////////////////
#if 1
        get_stereo_corner_correspondence(points_tracked_l[0], points_tracked_r[0],
            F, prevGray_r.cols, image_l, image_r);
#endif
        // undistorting the features tracked //////////////////////////////////////

        //    cv::undistortPoints(points_tracked[1], points_tracked[1], M1, D1,
        //    cv::Mat(), M1); cv::undistortPoints(pt1, pt1, M2, D2, cv::Mat(), M2);

    } // end of main for loop for optical flow
}

int main(int argc, char** argv)
{
    help(argv);
    int board_w = 7, board_h = 5;
    const char* board_list = "../calib_images/calib_images_list.txt";
    if (argc == 4) {
        board_list = argv[1];
        board_w    = atoi(argv[2]);
        board_h    = atoi(argv[3]);
    }
    StereoCalib(board_list, board_w, board_h, false);
    return 0;
}
