#include <iostream>
#include <sstream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

// configuration
int cam_id = 1;
int camera_width = 1280;
int camera_height = 480;

int board_width = 9;
int board_height = 6;


int nr_images = 20;
char folder[] = "/home/sgj/workspace/gaze-vergence-control/src/camera_calib/calib_images_elp_l_720p";

char intrinsics_fn[] = "elp_l_720p.xml";

bool need_calib = true;
bool use_cam = false;

int calib_from_files(
   int board_width,
   int board_height,
   int nr_images,
   char *folder,
   vector<vector<Point2f> > &image_points,
   vector<vector<Point3f> > &object_points) {

   int board_size = board_width * board_height;
   Size board_sz = Size(board_width, board_height);

   namedWindow("Calibration", CV_WINDOW_AUTOSIZE);

   for (int i = 1; i <= nr_images; i++) {

      stringstream ss;
      ss << folder << "/" << i << ".png";
      string fn = ss.str();

      cout << "retrieving image " << i << endl;

      Mat image = imread(fn.c_str());

      vector<cv::Point2f> corners;
      bool found = cv::findChessboardCorners(image, board_sz, corners);

      if (found) {

         image_points.push_back(corners);

         vector<cv::Point3f> object_point;

         for (int j = 0; j < board_size; j++) {

            float x = (float) (j / board_width);
            float y = (float) (j % board_width);
            float z = 0.0;
            Point3f p(x, y, z);
            object_point.push_back(p);

         }

         object_points.push_back(object_point);

         drawChessboardCorners(image, board_sz, corners, found);
      }


      imshow("Calibration", image);

      if ((waitKey(30)) == 27)
         return -1;
   }

   destroyWindow("Calibration");

   return 0;
}



int calib_from_cam(
   int cam_id,
   int camera_width,
   int camera_height,
   int board_width,
   int board_height,
   int nr_images,
   char *folder,
   vector<vector<Point2f> > &image_points,
   vector<vector<Point3f> > &object_points) {

   int board_size = board_width * board_height;
   Size board_sz = Size(board_width, board_height);


   VideoCapture capture(cam_id);
   if (!capture.isOpened()) {
      cout << "Couldn't open the camera\n";
      return -1;
   }

   capture.set(CV_CAP_PROP_FRAME_WIDTH, camera_width);
   capture.set(CV_CAP_PROP_FRAME_HEIGHT, camera_height);

   camera_width = static_cast<int>(capture.get(CV_CAP_PROP_FRAME_WIDTH));
   camera_height = static_cast<int>(capture.get(CV_CAP_PROP_FRAME_HEIGHT));

   cout << "Camera opened: " << camera_width << " * " << camera_height << endl;
   namedWindow("Calibration", CV_WINDOW_AUTOSIZE);


   // find corners on the chess board
   for (int i = 0, j = 0; image_points.size() < (size_t) nr_images; j++) {

      Mat image;
      capture.read(image);

      if (j % 100 == 0) {

         //image_size = image.size();
         //resize(image0, image, Size(), image_sf, image_sf, INTER_LINEAR);

         vector<cv::Point2f> corners;
         bool found = cv::findChessboardCorners(image, board_sz, corners);

         if (found) {

            image_points.push_back(corners);

            vector<Point3f> object_point;

            for (int j = 0; j < board_size; j++) {

               float x = (float) (j / board_width);
               float y = (float) (j % board_width);
               float z = 0.0;
               Point3f p(x, y, z);
               object_point.push_back(p);

            }

            object_points.push_back(object_point);

            i++;

            stringstream ss;
            ss << folder << "/" << i << ".png";
            string fn = ss.str();
            imwrite(fn.c_str(), image);

            cout << "image " << i << " obtained: " << fn << endl;

            drawChessboardCorners(image, board_sz, corners, found);
         }


      }

      imshow("Calibration", image);

      if ((waitKey(30)) == 27)
         return -1;
   }

   destroyWindow("Calibration");

   capture.release();

   return 0;
}


int main(int argc, char *argv[]) {

   cout << "start" << endl;

   if (need_calib) {

      vector<vector<Point2f> > image_points;
      vector<vector<Point3f> > object_points;

      Size image_size(camera_width, camera_height);

      if (use_cam) {

         calib_from_cam(cam_id, camera_width, camera_height,
                        board_width, board_height, nr_images, folder,
                        image_points, object_points);

      } else {

         calib_from_files(board_width, board_height, nr_images, folder,
                          image_points, object_points);
      }

      cout << "calibrating camera..." << flush;

      Mat intrinsic_matrix, distortion_coeffs;
      double err = calibrateCamera(
         object_points,
         image_points,
         image_size,
         intrinsic_matrix,
         distortion_coeffs,
         cv::noArray(),
         cv::noArray(),
         0);

      cout << "done. err = " << err << endl;

      FileStorage fs(intrinsics_fn, FileStorage::WRITE);
      fs << "camera_width" << image_size.width
         << "camera_height" << image_size.height
         << "intrinsic_matrix" << intrinsic_matrix
         << "distortion_coefficients" << distortion_coeffs;
      fs.release();

   }

   // loading camera parameters from file

   bool initDone = false;

   FileStorage fs;
   fs.open(intrinsics_fn, FileStorage::READ);
   camera_width = static_cast<int>(fs["camera_width"]);
   camera_height = static_cast<int>(fs["camera_height"]);
   Mat intrinsic_matrix_loaded;
   Mat distortion_coeffs_loaded;
   fs["intrinsic_matrix"] >> intrinsic_matrix_loaded;
   fs["distortion_coefficients"] >> distortion_coeffs_loaded;

   cout << "camera_width: " << camera_width << "\n";
   cout << "camera_height: " << camera_height << "\n";
   cout << "intrinsic matrix:" << intrinsic_matrix_loaded << "\n";
   cout << "distortion coefficients: " << distortion_coeffs_loaded << endl;

   Size image_size(camera_width, camera_height);
   Mat map1;
   Mat map2;
   initUndistortRectifyMap(
      intrinsic_matrix_loaded,
      distortion_coeffs_loaded,
      Mat(),
      intrinsic_matrix_loaded,
      image_size,
      CV_16SC2,
      map1,
      map2);

   namedWindow("Original", CV_WINDOW_AUTOSIZE);
   namedWindow("Undistorted", CV_WINDOW_AUTOSIZE);
   namedWindow("Left Img", CV_WINDOW_AUTOSIZE);
   namedWindow("Right Img", CV_WINDOW_AUTOSIZE);


   VideoCapture capture(cam_id);
   if (!capture.isOpened()) {
      cout << "Couldn't open the camera\n";
      return -1;
   }

   capture.set(CV_CAP_PROP_FRAME_WIDTH, camera_width);
   capture.set(CV_CAP_PROP_FRAME_HEIGHT, camera_height);

   // test
   // test1
   // test 2
   // test3

   Mat prevGray;

   while (true) {

      Mat image;
      Mat image_undistorted;
      cv::Rect  left_roi(0, 0 , camera_height, camera_width),
         right_roi(camera_height, camera_width, camera_height*2, camera_width*2);
      bool ret = capture.read(image);

      if (!ret) {
         break;
      }

      remap(image, image_undistorted, map1, map2, INTER_LINEAR,
            BORDER_CONSTANT, Scalar());

      cout << "image size = "  << image.size  <<"\n";
      Mat left_img = image_undistorted(Rect(0, 0, camera_width/2, camera_height));
      Mat right_img =  image_undistorted(Rect(camera_width/2, 0, camera_width/2,  camera_height));


      imshow("Original", image);
      imshow("Undistorted", image_undistorted);

      imshow("Left Img", left_img);
      imshow("Right Img", right_img);
      // Mat l_corners, r_corners;
      vector<Point2f> l_corners[2], r_corners[2];

      int maxCorners = 500;
      double qualityLevel = 0.25, minDistance = 10.0;
      Mat l_gray, r_gray;
      left_img.copyTo(l_gray);
      cvtColor(image, l_gray, COLOR_BGR2GRAY);

      right_img.copyTo(r_gray);
      cvtColor(image, r_gray, COLOR_BGR2GRAY);

      TermCriteria termcrit(TermCriteria::COUNT|TermCriteria::EPS,20,0.03);

      if(!initDone) {

         Size subPixWinSize(10,10);
         cv::goodFeaturesToTrack(l_gray, l_corners[0], maxCorners, qualityLevel, minDistance);
         cornerSubPix(l_gray, l_corners[0], subPixWinSize, Size(-1,-1), termcrit);
         l_corners[1] = l_corners[0];
         cout << "Number of features in left image = "  << l_corners[1].size()  << "\n";

         cv::goodFeaturesToTrack(r_gray, r_corners[0], maxCorners, qualityLevel, minDistance);
         cout << "Number of features in right image = "  << r_corners[0].size()  << "\n";
         initDone = true;
      }
      if(prevGray.empty())
         l_gray.copyTo(prevGray);
      vector<uchar> status;
      vector<float> err;
      Size winSize(31,31);
      l_gray.convertTo(l_gray, CV_32F);
      cout << "image shape" << l_gray.size()  << "\n";
      cout << "Depth = " << l_gray.depth() << ", Win size width, height =" << winSize.width << " " << winSize.height << "\n";
      if(l_gray.depth() == CV_8U )
          cout << "depth = CV_8U" << '\n' ;
      calcOpticalFlowPyrLK(l_gray, l_gray, l_corners[0], l_corners[0], status, err); //, winSize, 3, termcrit, 0, 0.001);
      if ((waitKey(30)) == 27) {
         break;
      }
   }

   capture.release();

   return 0;
}
