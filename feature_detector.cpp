/**
 *   \file feature_detector.cpp
 *   \brief Contains the abstraction
 *          feature/corner detection.
 *  Detailed description
 *
 */

#include <iostream>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class KeypointDetector
{
 public:
  //! Default constructor
  KeypointDetector();

  //! Copy constructor
  KeypointDetector(const KeypointDetector &other);

  //! Move constructor
  KeypointDetector(KeypointDetector &&other) noexcept;

  //! Destructor
  virtual ~KeypointDetector() noexcept;

  //! Copy assignment operator
  KeypointDetector& operator=(const KeypointDetector &other);

  //! Move assignment operator
  KeypointDetector& operator=(KeypointDetector &&other) noexcept;

 protected:
 private:
};

void FAST_keypoint_detector(vector<KeyPoint>& keypointsCorners, Mat& grayImage, double sigma = 1, int thresholdCorner = 40) {

  // applying gaussian blur to remove some noise,if present
  GaussianBlur(grayImage, grayImage, Size(), sigma, sigma);

  // applying FAST key point detector
  FAST(grayImage,keypointsCorners,thresholdCorner,true);
  if(keypointsCorners.size() > 0) {
    cout << " number of keypoints"  <<keypointsCorners.size() << endl;
  }

}

int main() {

  Mat img = imread("/home/sgj/workspace/gaze-vergence-control/src/visual_odometry/10.png", IMREAD_UNCHANGED);
  if( img.empty()) {
    cout << "File not available" << endl;
    return -1;
  }

  Mat grayImage;
  // converting color to gray image
  if(img.channels() >2) {
    cvtColor( img, grayImage, CV_BGR2GRAY );
  }
  else {
    grayImage =  img;
  }

  vector<KeyPoint> keypointsCorners;

  FAST_keypoint_detector(keypointsCorners, grayImage );

    // Drawing a circle around corners
  for(int i = 0; i < keypointsCorners.size(); i++ ) {
    circle( grayImage, keypointsCorners.at(i).pt, 5,  Scalar(0), 2, 8, 0 );
  }

  cv::namedWindow("Corners Display Image");
  cv::imshow("Corners Display Image", grayImage);
  cvWaitKey(300000);
  cvDestroyWindow("Corners Display Image");

}
